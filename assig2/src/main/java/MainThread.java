import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class MainThread extends Thread{
    private ArrayList<Queue> queues;
    private GUI gui;

    public MainThread(){
        queues=new ArrayList<Queue>();
        gui = new GUI("attempt");
    }

    public void startQueues(int n){
        for(int i=0;i<n;i++){
            Queue queue = new Queue();
            queue.setId(i+1);
            queues.add(queue);
        }
        for(Queue queue:queues){
            Thread t = new Thread(queue);
            t.start();
        }
    }

    public long totalWaitingTimePerQueue(Queue queue){
        long total=0;
        for(Customer c:queue.getQueue()){
            total+=c.getServiceTime();
        }
        return total;
    }

    public Queue minWaitingTime(ArrayList<Queue> queuesList){
        Queue min = queuesList.get(0);
        for(Queue q:queuesList){
            if(totalWaitingTimePerQueue(q)<totalWaitingTimePerQueue(min)){
                min = q;
            }
        }
        return min;
    }

    public boolean existCustomer(ArrayList<Queue> queues1){

        for(Queue q:queues1){
            if(!q.getQueue().isEmpty())
                return false;
        }
        return true;
    }

    public static String getSystemTime(){
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        return sdf.format(cal.getTime());
    }

    public void printQueues(int noOfQueues){
        if(noOfQueues == 1) {
            gui.setQueue1("Queue #" + queues.get(0).getQueueId()+":   "+queues.get(0));
        }
        if(noOfQueues == 2){
            gui.setQueue1("Queue #" + queues.get(0).getQueueId()+":   "+queues.get(0));
            gui.setQueue2("Queue #" + queues.get(1).getQueueId()+":   "+queues.get(1));
        }
        if(noOfQueues == 3){
            gui.setQueue1("Queue #" + queues.get(0).getQueueId()+":   "+queues.get(0));
            gui.setQueue2("Queue #" + queues.get(1).getQueueId()+":   "+queues.get(1));
            gui.setQueue3("Queue #" + queues.get(2).getQueueId()+":   "+queues.get(2));
        }
        if(noOfQueues == 4){
            gui.setQueue1("Queue #" + queues.get(0).getQueueId()+":   "+queues.get(0));
            gui.setQueue2("Queue #" + queues.get(1).getQueueId()+":   "+queues.get(1));
            gui.setQueue3("Queue #" + queues.get(2).getQueueId()+":   "+queues.get(2));
            gui.setQueue4("Queue #" + queues.get(3).getQueueId()+":   "+queues.get(3));
        }
        if(noOfQueues == 5){
            gui.setQueue1("Queue #" + queues.get(0).getQueueId()+":   "+queues.get(0));
            gui.setQueue2("Queue #" + queues.get(1).getQueueId()+":   "+queues.get(1));
            gui.setQueue3("Queue #" + queues.get(2).getQueueId()+":   "+queues.get(2));
            gui.setQueue4("Queue #" + queues.get(3).getQueueId()+":   "+queues.get(3));
            gui.setQueue5("Queue #" + queues.get(4).getQueueId()+":   "+queues.get(4));
        }
    }

    public void run() {
        int id = 1;
        float avgWaitingTime = 0;
        float avgServiceTime = 0;

        long t = System.currentTimeMillis();
        long time = System.currentTimeMillis();
        long end = t + gui.getTime() * 1000;
        int max = 0;
        String peak = "";
        Queue q = new Queue();
        startQueues(gui.getNumber());
        try {
            while (t < end) {
                Customer customer = new Customer();
                customer.setId(999);
                t = System.currentTimeMillis();
                long arrival = new Queue().generateRandomArrivalIneterval(gui.getArrival1() * 1000, gui.getArrival2() * 1000);
                for(Queue queue:queues){
                    if(queue.equals(minWaitingTime(queues))){
                        if (queue.getQueue().isEmpty())
                            queue.setEmptyTime(arrival);
                    }
                }
                if (t > time + arrival) {
                    time = System.currentTimeMillis();
                    customer = new Queue().generateRandomCustomer((int) gui.getService1() * 1000, (int) gui.getService2() * 1000);
                    avgWaitingTime += arrival;
                    avgServiceTime += customer.getServiceTime();
                    customer.setArrivalTime(t + arrival);
                    customer.setId(id);
                    id++;
                    q = minWaitingTime(queues);
                    q.addInQueue(customer);
                }
                Calendar cal = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
                int customers = 0;
                for (Queue queue : queues) {
                    customers += queue.getQueue().size();

                }
                if (customers > max) {
                    max = customers;
                    peak = sdf.format(cal.getTime());
                }
                sleep(1000);
                gui.setSystemTime(sdf.format(cal.getTime()));
                if (customer.getId() != 999)
                    GUI.addLogEvent(sdf.format(cal.getTime()) + " -> Added customer: " + customer + "to Queue " + q.getQueueId() + "\n");
                printQueues(gui.getNumber());

            }
            while (!existCustomer(queues)) {
                sleep(1000);
                Calendar cal = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
                gui.setSystemTime(sdf.format(cal.getTime()));
                printQueues(gui.getNumber());
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        id--;

        avgServiceTime = avgServiceTime / id / 1000;
        avgWaitingTime = avgWaitingTime / id / 1000;

        gui.setPeak(peak);
        gui.setAvgServiceTime(avgServiceTime);
        gui.setWaitingTime(avgWaitingTime);
        if(gui.getNumber() == 1) {
            gui.setEmpty1(String.valueOf(queues.get(0).getEmptyTime()/1000));
            gui.setEmpty2("Never used!");
            gui.setEmpty3("Never used!");
            gui.setEmpty4("Never used!");
            gui.setEmpty5("Never used!");
        }
        if(gui.getNumber() == 2){
            gui.setEmpty1(String.valueOf(queues.get(0).getEmptyTime()/1000));
            gui.setEmpty2(String.valueOf(queues.get(1).getEmptyTime()/1000));
            gui.setEmpty3("Never used!");
            gui.setEmpty4("Never used!");
            gui.setEmpty5("Never used!");
        }
        if(gui.getNumber() == 3){
            gui.setEmpty1(String.valueOf(queues.get(0).getEmptyTime()/1000));
            gui.setEmpty2(String.valueOf(queues.get(1).getEmptyTime()/1000));
            gui.setEmpty3(String.valueOf(queues.get(2).getEmptyTime()/1000));
            gui.setEmpty4("Never used!");
            gui.setEmpty5("Never used!");
        }
        if(gui.getNumber() == 4){
            gui.setEmpty1(String.valueOf(queues.get(0).getEmptyTime()/1000));
            gui.setEmpty2(String.valueOf(queues.get(1).getEmptyTime()/1000));
            gui.setEmpty3(String.valueOf(queues.get(2).getEmptyTime()/1000));
            gui.setEmpty4(String.valueOf(queues.get(3).getEmptyTime()/1000));
            gui.setEmpty5("Never used!");
        }
        if(gui.getNumber() == 5){
            gui.setEmpty1(String.valueOf(queues.get(0).getEmptyTime()/1000));
            gui.setEmpty2(String.valueOf(queues.get(1).getEmptyTime()/1000));
            gui.setEmpty3(String.valueOf(queues.get(2).getEmptyTime()/1000));
            gui.setEmpty4(String.valueOf(queues.get(3).getEmptyTime()/1000));
            gui.setEmpty5(String.valueOf(queues.get(4).getEmptyTime()/1000));
        }
    }
    public void init(){
        gui.addStartButtonActionListener(e->{
            GUI.log.setVisible(true);
            start();

        });
    }

    public static void main(String[] args) {
       new MainThread().init();
    }
}
