import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;


public class GUI extends JFrame {
    private JLabel arrivingTime;
    private JTextField arrival1;
    private JTextField arrival2;
    private JLabel serviceTime;
    private JTextField service1;
    private JTextField service2;
    private JLabel noOfQueues;
    private JTextField number;
    private JLabel simulationTime;
    private static JTextField time;
    private JLabel systemTime;
    private JLabel Queue1;
    private JLabel Queue2;
    private JLabel Queue3;
    private JLabel Queue4;
    private JLabel Queue5;
    private JButton start;
    public static JTextArea log;
    private JLabel waitingTime;
    private JLabel avgServiceTime;
    private JLabel empty1;
    private JLabel empty2;
    private JLabel empty3;
    private JLabel empty4;
    private JLabel empty5;
    private JLabel peak;

    public String getEmpty1() {
        return empty1.getText();
    }

    public void setEmpty1(String empty1) {
        this.empty1.setText(this.empty1.getText()+empty1);
    }

    public String getEmpty2() {
        return empty2.getText();
    }

    public void setEmpty2(String empty2) {
        this.empty2.setText(this.empty2.getText()+empty2);
    }

    public String getEmpty3() {
        return empty3.getText();
    }

    public void setEmpty3(String empty3) {
        this.empty3.setText(this.empty3.getText()+empty3);
    }

    public String getEmpty4() {
        return empty4.getText();
    }

    public void setEmpty4(String empty4) {
        this.empty4.setText(this.empty4.getText()+empty4);
    }

    public String getEmpty5() {
        return empty5.getText();
    }

    public void setEmpty5(String empty5) {
        this.empty5.setText(this.empty5.getText()+empty5);
    }

    public GUI(String s) throws HeadlessException {

        setTitle(s);
        setSize(1920,600);

        Font f = new Font("serif", Font.PLAIN, 30);

        Font f1 = new Font("serif", Font.PLAIN, 50);

        JPanel panel = new JPanel();
        panel.setVisible(true);
        panel.setEnabled(true);
        panel.setLayout(null);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        setContentPane(panel);


        JLabel result = new JLabel("RESULTS");
        result.setFont(f);
        result.setHorizontalAlignment(JLabel.CENTER);
        result.setBounds(1550,70,320,50);
        result.setVisible(true);
        panel.add(result);

        empty1 = new JLabel("Empty time for Queue 1: ");
        empty1.setBounds(1540,260,320,30);
        empty1.setVisible(true);
        empty1.setHorizontalAlignment(JLabel.LEFT);
        panel.add(empty1);

        empty2 = new JLabel("Empty time for Queue 2: ");
        empty2.setBounds(1540,300,320,30);
        empty2.setVisible(true);
        empty2.setHorizontalAlignment(JLabel.LEFT);
        panel.add(empty2);

        empty3 = new JLabel("Empty time for Queue 3: ");
        empty3.setBounds(1540,340,320,30);
        empty3.setVisible(true);
        empty3.setHorizontalAlignment(JLabel.LEFT);
        panel.add(empty3);

        empty4 = new JLabel("Empty time for Queue 4: ");
        empty4.setBounds(1540,380,320,30);
        empty4.setVisible(true);
        empty4.setHorizontalAlignment(JLabel.LEFT);
        panel.add(empty4);

        empty5 = new JLabel("Empty time for Queue 5: ");
        empty5.setBounds(1540,420,320,30);
        empty5.setVisible(true);
        empty5.setHorizontalAlignment(JLabel.LEFT);
        panel.add(empty5);

        avgServiceTime = new JLabel("Average service time: ");
        avgServiceTime.setBounds(1540,140,320,30);
        avgServiceTime.setVisible(true);
        avgServiceTime.setHorizontalAlignment(JLabel.LEFT);
        panel.add(avgServiceTime);

        waitingTime = new JLabel("Average waiting time before entering a queue: ");
        waitingTime.setBounds(1540,180,360,30);
        waitingTime.setVisible(true);
        waitingTime.setHorizontalAlignment(JLabel.LEFT);
        panel.add(waitingTime);

        peak = new JLabel("The peak hour was: ");
        peak.setBounds(1540,220,320,30);
        peak.setVisible(true);
        peak.setHorizontalAlignment(JLabel.LEFT);
        panel.add(peak);

        JLabel please = new JLabel("Welcome to the simulation of the queues system");
        please.setFont(f);
        please.setBounds(20,15,1880,35);
        please.setHorizontalAlignment(JLabel.CENTER);
        please.setVisible(true);
        panel.add(please);

        arrivingTime = new JLabel("Please insert the interval of time you want the customers to randomly enter a queue:");
        arrivingTime.setBounds(20,70,500,20);
        arrivingTime.setHorizontalAlignment(JLabel.CENTER);
        arrivingTime.setVisible(true);
        panel.add(arrivingTime);

        arrival1 = new JTextField();
        arrival1.setBounds(90, 110, 100,20);
        arrival1.setHorizontalAlignment(JTextField.CENTER);
        arrival1.setVisible(true);
        panel.add(arrival1);

        arrival2 = new JTextField();
        arrival2.setBounds(280, 110, 100,20);
        arrival2.setHorizontalAlignment(JTextField.CENTER);
        arrival2.setVisible(true);
        panel.add(arrival2);

        serviceTime = new JLabel("Please insert the interval of time you want the customer`s service time to be situated in:");
        serviceTime.setBounds(20,150,500,20);
        serviceTime.setHorizontalAlignment(JLabel.CENTER);
        serviceTime.setVisible(true);
        panel.add(serviceTime);

        service1 = new JTextField();
        service1.setBounds(90, 190, 100,20);
        service1.setHorizontalAlignment(JTextField.CENTER);
        service1.setVisible(true);
        panel.add(service1);

        service2 = new JTextField();
        service2.setBounds(280, 190, 100,20);
        service2.setHorizontalAlignment(JTextField.CENTER);
        service2.setVisible(true);
        panel.add(service2);

        noOfQueues = new JLabel("Please insert the number of queues you wish the simulation to use:                   ");
        noOfQueues.setBounds(20,230,500,20);
        noOfQueues.setHorizontalAlignment(JLabel.CENTER);
        noOfQueues.setVisible(true);
        panel.add(noOfQueues);

        number = new JTextField();
        number.setBounds(180, 270, 100,20);
        number.setHorizontalAlignment(JTextField.CENTER);
        number.setVisible(true);
        panel.add(number);

        simulationTime = new JLabel("Please insert the period of time you wish the simulation to run:                   ");
        simulationTime.setBounds(20,320,500,20);
        simulationTime.setHorizontalAlignment(JLabel.CENTER);
        simulationTime.setVisible(true);
        panel.add(simulationTime);

        time = new JTextField();
        time.setBounds(180, 360, 100,20);
        time.setHorizontalAlignment(JTextField.CENTER);
        time.setVisible(true);
        panel.add(time);

        systemTime = new JLabel();
        systemTime.setBounds(20,430,420,60);
        systemTime.setFont(f1);
        systemTime.setHorizontalAlignment(JLabel.CENTER);
        systemTime.setVisible(true);
        panel.add(systemTime);
        setVisible(true);

        start = new JButton("START");
        start.setBounds(750,460,400,70);
        start.setHorizontalAlignment(JLabel.CENTER);
        start.setVisible(true);
        start.setFont(f1);
        panel.add(start);

        Queue1 = new JLabel("Queue #1: CLOSED");
        Queue1.setBounds(560,100,550,30);
        Queue1.setHorizontalAlignment(JLabel.LEFT);
        Queue1.setVisible(true);
        //Queue1.setFont(f);
        panel.add(Queue1);

        Queue2 = new JLabel("Queue #2: CLOSED");
        Queue2.setBounds(560,160,550,30);
        Queue2.setHorizontalAlignment(JLabel.LEFT);
        Queue2.setVisible(true);
        //Queue1.setFont(f);
        panel.add(Queue2);

        Queue3 = new JLabel("Queue #3: CLOSED");
        Queue3.setBounds(560,220,550,30);
        Queue3.setHorizontalAlignment(JLabel.LEFT);
        Queue3.setVisible(true);
        //Queue1.setFont(f);
        panel.add(Queue3);

        Queue4 = new JLabel("Queue #4: CLOSED");
        Queue4.setBounds(560,280,550,30);
        Queue4.setHorizontalAlignment(JLabel.LEFT);
        Queue4.setVisible(true);
        //Queue1.setFont(f);
        panel.add(Queue4);

        Queue5 = new JLabel("Queue #5: CLOSED");
        Queue5.setBounds(560,340,550,30);
        Queue5.setHorizontalAlignment(JLabel.LEFT);
        Queue5.setVisible(true);
        //Queue1.setFont(f);
        panel.add(Queue5);

        log = new JTextArea();
        log.setBounds(1170,65,340,450);
        log.setVisible(false);
        //Queue1.setFont(f);
        panel.add(log);

        JScrollPane sp = new JScrollPane(log);
        sp.setBounds(1170,65,340,450);
        if(log.isVisible())
            sp.setVisible(true);
        panel.add(sp);

    }

    public long getArrival1() {
        return Integer.parseInt(arrival1.getText());
    }

    public String getPeak() {
        return peak.getText();
    }

    public void setPeak(String peak) {
        this.peak.setText(this.peak.getText()+peak);
    }

    public void setArrival1(String s) {
        this.arrival1.setText(s);
    }

    public long getArrival2() {
        return Integer.parseInt(arrival2.getText());
    }

    public void setArrival2(String arrival2) {
        this.arrival2.setText(arrival2) ;
    }


    public String getWaitingTime() {
        return waitingTime.getText();
    }

    public void setWaitingTime(Float waitingTime) {
        this.waitingTime.setText(this.waitingTime.getText()+waitingTime);
    }


    public long getService1() {
        return Integer.parseInt(service1.getText());
    }

    public void setService1(String s) {
        this.service1.setText(s);
    }

    public long getService2() {
        return Integer.parseInt(service2.getText());
    }

    public void setService2(String s) {
        this.service2.setText(s);
    }

    public int getNumber() {
        return Integer.parseInt(number.getText());
    }

    public void setNumber(String s) {
        this.number.setText(s);
    }

    public static long getTime() {
        return Integer.parseInt(time.getText());
    }

    public void setTime(String s) {
        this.time.setText(s);
    }

    public JLabel getSystemTime() {
        return systemTime;
    }

    public void setSystemTime(String s) {
        systemTime.setText(s);
    }

    public void setQueue1(String s) {
        this.Queue1.setText(s);
    }
    public void setQueue2(String s) {
        this.Queue2.setText(s);
    }
    public void setQueue3(String s) {
        this.Queue3.setText(s);
    }
    public void setQueue4(String s) {
        this.Queue4.setText(s);
    }

    public String getAvgServiceTime() {
        return avgServiceTime.getText();
    }

    public void setAvgServiceTime(float s) {
        this.avgServiceTime.setText(avgServiceTime.getText() + s);
    }

    public void setQueue5(String s) {
        this.Queue5.setText(s);
    }

    public void addStartButtonActionListener(final ActionListener actionListener){
        start.addActionListener(actionListener);
    }

    public static void addLogEvent(String s){
        log.setText(log.getText() + s);
    }

    public static void main(String[] args) {
        new GUI("test");
    }
}
