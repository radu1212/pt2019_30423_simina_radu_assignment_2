import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class Queue implements Runnable {

    private int id;
    private ArrayList<Customer> queue;
    private long emptyTime;

    public int getQueueId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String printQueue(){
        StringBuilder s = new StringBuilder();
        s.append("Queue "+id+": ");
        for (Customer c : queue) {
            s.append(c);
            s.append(" ");
        }
        return s.toString();
    }

    public Queue() {
        this.queue = new ArrayList<Customer>();
    }

    public synchronized void addInQueue(Customer customer){
        queue.add(queue.size(),customer);
    }

    public synchronized void removeFromQueue(){
        queue.remove(0);
    }

    public synchronized Customer generateRandomCustomer(int minimumServiceTime, int maximumServiceTime){
        int serviceTime = minimumServiceTime + (int)(Math.random() * ((maximumServiceTime - minimumServiceTime) + 1));
        Customer customer = new Customer();
        customer.setServiceTime(serviceTime);

        return customer;
    }

    public synchronized long generateRandomArrivalIneterval(long min,long max){
        long serviceTime = min + (int)(Math.random() * ((max - min) + 1));
        return serviceTime;
    }

    public ArrayList<Customer> getQueue() {
        return queue;
    }

    @Override
    public String toString() {
        String s = "";
        for (Customer c : queue) {
            s = s + c.toString();
        }
        return s;
    }

    public synchronized int getServiceTime(){
        int total = 0;
        for (Customer c : queue) {
            total = total + c.getServiceTime();
        }
        return total;
    }


    public long getEmptyTime() {
        return emptyTime;
    }

    public void setEmptyTime(long emptyTime) {
        this.emptyTime += emptyTime + 1;
    }

    public void run() {
        emptyTime = 0;
        while(true) {
            try {
                Thread.sleep(50);
                if(!queue.isEmpty()) {
                    Thread.sleep(queue.get(0).getServiceTime());
                    GUI.addLogEvent( MainThread.getSystemTime() +     " -> Removed customer: "+queue.get(0) + " from Queue "+ id + "\n");
                    removeFromQueue();
                }

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {

        Queue queue = new Queue();
        Thread thread = new Thread(queue);
        thread.start();
    }


}
